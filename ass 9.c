#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main (){
//file open in write mode
	FILE*pt;
	char ch[100];
	pt=fopen("assignment9.txt","w");
	fprintf(pt,"UCSC is one of the leading institutes in Sri Lanka for computing studies.");
	fclose(pt);

//file open in read mode
	pt=fopen("assignment9.txt","r");
	printf("%s",fgets(ch,100,pt));
	fclose(pt);

//file open in append mode
	pt=fopen("assignment9.txt","a");
	fprintf(pt,"\nUCSC offers undergraduate and postgraduate level courses aiming a range of computing fields.");
	fclose(pt);

return 0;
}
